# ResultsDB

ResultsDB is a results store engine for (not only) FedoraQA tools.

## Repositories

* ResultsDB Frontend - [Bitbucket GIT repo](https://bitbucket.org/rajcze/resultsdb_frontend)
* ResultsDB Client Library - [Bitbucket GIT repo](https://bitbucket.org/rajcze/resultsdb_api)
